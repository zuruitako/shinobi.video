[
    {
        "description":"Adding an H.264/H.265 Camera",
        "blocks":[
            {
                "info":"Adding an H.264 camera is the easiest Input Type for Shinobi to consume.<br><br><iframe width='560' height='315' src='https://www.youtube.com/embed/bw4TkzvVjXI' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>",
                "list":[
                    "<a href='https://www.youtube.com/watch?v=bw4TkzvVjXI'><b>Video</b> : How to Add a Camera</a>",
                    "<a href='https://shinobi.video/articles/2017-08-22-view-h264-and-h265-streams-in-the-browser-with-shinobi-and-maintain-low-cpu-use'><b>Article</b> : View H.264 and H.265 streams in the browser with Shinobi and maintain low CPU use</a>",
                    "<a href='https://shinobi.video/articles/2017-07-29-how-i-optimized-my-rtsp-camera'><b>Article</b> : How I optimized my RTSP camera</a>"
                ]
            },
            {
                "text":"About Monitor Modes",
                "info":"There are 4 Modes that you can choose from for a camera.",
                "list":[
                    "<b>Disabled</b> : The monitor is not loaded to be used by Shinobi.",
                    "<b>Watch-Only</b> : Shinobi is Watching the camera stream. It will not be recording unless ordered to do so by a user or an Event.",
                    "<b>Record</b> : Continuous Recording. Event-based recordings are disabled when the monitor is in this mode.",
                    "<b>Idle</b> : Disabled but loaded and ready for Shinobi to interact with the monitor variables."
                ]
            }
        ]
    },
    {
        "description":"Adding an MJPEG Camera",
        "blocks":[
            {
                "info":"MJPEG is the old school way of streaming. Shinobi supports it but it takes a little bit of know-how. Here's that know-how. If your camera is capable of providing an H.264 Stream I suggest you take a look at the <a href='#content-adding-an-h264h265-camera'>links above</a> first.<br><br>The second link provided can show you how to add <b>Audio</b> to your MJPEG stream input.",
                "list":[
                    "<a href='https://shinobi.video/articles/2018-09-19-how-to-add-an-mjpeg-camera'><b>Article</b> : How to add an MJPEG Camera</a>",
                    "<a href='https://shinobi.video/articles/2018-09-23-how-to-use-additional-input-feeds'><b>Article</b> : How to use Additional Input Feeds</a>"
                ]
            }
        ]
    },
    {
        "description":"Adding a Streamer Camera",
        "blocks":[
            {
                "info":"Streamer cameras are a special P2P type connection that utilizes devices capable of enabling WebRTC protocol.<br><br><iframe width='560' height='315' src='https://www.youtube.com/embed/7pO1l87iNMY' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>",
                "list":[
                    "<a href='https://shinobi.video/articles/2018-06-30-difference-between-shinobi-streamer-v1-and-dashcam-streamer-v2'><b>Article</b> : Difference between Shinobi Streamer v1 and Dashcam (Streamer v2)</a>",
                    "<a href='https://www.youtube.com/embed/7pO1l87iNMY'><b>Video</b> : How to add a Webcam to Shinobi through the Browser (WebRTC and Shinobi Streamer)</a>"
                ]
            }
        ]
    },
    {
        "description":"Setting up PTZ Controls (Point Tilt Zoom)",
        "blocks":[
            {
                "info":"\"PTZ is an abbreviation for pan, tilt and zoom and reflects the movement options of the camera. Other types of cameras are ePTZ or virtual pan-tilt-zoom (VPTZ) where a high-resolution camera digitally zooms and pans into portions of the image, with no physical camera movement. Ultra-low bandwidth surveillance streaming technologies use VPTZ to stream user-defined areas in higher quality without increasing overall bandwidth usage.\"<br><small>Wikipedia - <a href='https://en.wikipedia.org/wiki/Pan%E2%80%93tilt%E2%80%93zoom_camera' target='_blank'>Pan–tilt–zoom camera</a></small><br><br><iframe width='560' height='315' src='https://www.youtube.com/embed/zjs-8LZDc1I' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>",
                "list":[
                    "<a href='https://shinobi.video/articles/2018-11-24-how-to-setup-ptz-in-shinobi'><b>Article</b> : How to setup PTZ in Shinobi</a>"
                ]
            }
        ]
    },
    {
        "description":"Setting up Motion Detection",
        "blocks":[
            {
                "list":[
                    "<a href='https://shinobi.video/articles/2018-10-06-how-to-use-motion-detection'><b>Article</b> : How to use Motion Detection</a>",
                    "<a href='https://shinobi.video/articles/2019-02-26-how-to-use-smtpbased-event-triggering-in-shinobi'><b>Article</b> : How to use SMTP-based Event Triggering</a>",
                    "<a href='https://shinobi.video/articles/2019-02-23-how-to-use-ftpbased-event-triggering-in-shinobi'><b>Article</b> : How to use FTP-based Event Triggering</a>"
                ]
            }
        ]
    },
    {
        "description":"Cloud Back Up for Videos",
        "blocks":[
            {
                "info":"Amazon S3 is not the only cloud back up method. There is Backblaze B2 and WebDAV support as well. The premise in their setup is similar to Amazon S3.",
                "list":[
                    "<a href='https://shinobi.video/articles/2018-09-24-how-to-backup-videos-to-amazon-s3-cloud-storage'><b>Article</b> : How to backup videos to Amazon S3 Cloud Storage</a>"
                ]
            }
        ]
    },
    {
        "description":"Setup E-Mail Notifications",
        "blocks":[
            {
                "info":"Shinobi uses Nodemailer for mailing functions. The mail object in conf.json is the object used to initiate Nodemailer.",
                "list":[
                    "<a href='https://shinobi.video/articles/2017-08-17-how-to-setup-mailing-in-shinobi'><b>Article</b> : How to setup mailing in Shinobi</a>",
                    "<a href='https://nodemailer.com/smtp/'><b>Nodemailer</b> : SMTP transport setup</a>",
                    "<a href='https://nodemailer.com/transports/'><b>Nodemailer</b> : Other transport</a>"
                ]
            }
        ]
    },
    {
        "description":"Learning about different Account Types",
        "blocks":[
            {
                "info":"There are 3 distinct user levels in Shinobi. Superuser, Admin, and Sub-Account. In essence, Superuser is for creating Admin accounts which are masters of \"Groups\". Admins are for creating Monitors and Sub-Accounts. Sub-Accounts are created by the Admin with limited privileges when cameras are shared with other people.",
                "list":[
                    "<a href='https://shinobi.video/articles/2017-08-15-difference-between-superuser-admin-and-subaccount'><b>Article</b> : Difference between Superuser, Admin, and Sub-Account</a>",
                    "<a href='https://shinobi.video/articles/2018-04-29-how-to-modify-superuser-access'><b>Article</b> : How to modify Superuser access</a>"
                ]
            }
        ]
    },
    {
        "description":"How to Update Shinobi",
        "blocks":[
            {
                "info":"Shinobi uses <code>git</code> to manage changes, version numbers, and updates. The following commands need to be run from in side the Shinobi directory.",
                "blocks": [
                    {
                        "text": "Get Version Number",
                        "code": "git show"
                    },
                    {
                        "text": "View Git History",
                        "code": "git logs"
                    }
                ],
                "list":[
                    "<a href='https://shinobi.video/articles/2018-05-15-how-to-update-shinobi-manually-from-terminal'><b>Article</b> : How to Manually Update Shinobi</a>"
                ]
            }
        ]
    },
    {
        "description":"Changing Languages",
        "blocks":[
            {
                "info":"Shinobi uses Yandex to create Translations automatically. Of course these translations are far from perfect. If you see an incorrect translation or would like to add a language please consider making a Merge Request.",
                "list":[
                    "<a href='https://shinobi.video/articles/2018-02-22-how-to-make-or-update-a-language-file-for-shinobi'><b>Article</b> : How to make or update a language file for Shinobi</a>"
                ]
            }
        ]
    }
]
