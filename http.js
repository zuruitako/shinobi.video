//Fail safe
process.on('uncaughtException', function (err) {
    console.error('uncaughtException',err);
});
//variables
var https = require('https');
var http = require('http');
var moment = require('moment');
var request = require('request');
var exec = require('child_process').exec;
var express = require('express');
var nodemailer = require('nodemailer');
var fs = require('fs');
var nedb = require('nedb')
var app = express()
var config = require('./conf.json')
var bodyParser = require('body-parser')
var jsonfile = require('jsonfile')
var cameraBrands=null;
if(!config.port){config.port=80}
if(!config.adminIP){config.adminIP=[]}
if(config.mail){
    var nodemailer = require('nodemailer').createTransport(config.mail);
}
// var eventTracker = require('./libs/eventTracker')(config)
s={cachedCameras:{},s:JSON.stringify};

// DISCORD >
if(config.mailBotToken){
    const Discord = require("discord.js");
    const mailBot = new Discord.Client();
    mailBot.on('ready', () => {
        console.log(`Logged in as ${mailBot.user.tag}!`)
    });
    s.sendDiscordMail = function(dataBody){
        if(!dataBody)dataBody = {};
        var sendBody = Object.assign({
            color: 3447003,
            author: {
              name: "Shinobi Guest",
            },
            title: 'New Message on shinobi.video',
            description: "",
            fields: [],
            timestamp: new Date(),
            footer: {
              icon_url: "https://shinobi.video/libs/assets/icon/apple-touch-icon-152x152.png",
              text: "Shinobi Systems"
            }
        },dataBody)
        mailBot.channels.get(config.mailBotChannel).send({embed: sendBody})
    }
    mailBot.login(config.mailBotToken)
}else{
    s.sendDiscordMail = function(dataBody){
        console.log('No Discord Token',dataBody)
    }
}
// DISCORD />

//vars
s.database={}

s.closeJsonResponse = function(res,endData){
    res.setHeader('Content-Type', 'application/json')
    res.end(s.prettyPrint(endData))
}
s.prettyPrint = function(obj){return JSON.stringify(obj,null,3)}
s.database.contact = new nedb({ filename: __dirname+'/database/contact.json', autoload: true })
s.database.rebate = new nedb({ filename: __dirname+'/database/rebates.json', autoload: true })
s.database.returns = new nedb({ filename: __dirname+'/database/returns.json', autoload: true })
//functions
s.file_get_contents=function(target){
    return fs.readFileSync(target)
}
s.dir={
    web:__dirname+'/web',
    web_pages:__dirname+'/web/pages/',
    doc_pages:__dirname+'/web/docs/'
}
s.authIP = function(req){
    req.body.ip = req.headers['cf-connecting-ip'] || req.headers["CF-Connecting-IP"] || req.headers["'x-forwarded-for"] || req.connection.remoteAddress
    req.ret={ok:false}
    var isAdmin=false
    config.adminIP.forEach(function(v){
        if(req.body.ip.indexOf(v)>-1){
            isAdmin=true
        }
    })
    return isAdmin
}
app.use('/', express.static(process.cwd() + '/web'));
app.get('/medium', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.end(s.s(req.query))
})
app.set('views', __dirname + '/web');
app.set('view engine', 'ejs');
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
app.all('/webhook', function(req, res) {
    console.log('body',req.body)
    console.log('query',req.query)
})
app.get('/.well-known/apple-developer-merchantid-domain-association', function(req, res) {
    res.sendFile(__dirname+'/web/verifiers/apple-developer-merchantid-domain-association')
})
//undefined
app.get(['/undefined','/robots.txt'], function(req, res) {
    res.end('')
});
//favicon
app.get('/favicon.ico', function(req, res) {
    fs.createReadStream(__dirname+'/web/libs/assets/icon/favicon.ico').pipe(res).end()
});
//donations
app.get('/getOnlineVisitors', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    var controller=http
    if(config.onlineVisitorsURL.indexOf('https://')>-1){
        controller=https
    }
    controller.request(config.onlineVisitorsURL, function(data) {
        data.setEncoding('utf8');
        var chunks='';
        data.on('data', (chunk) => {
          chunks+=chunk;
        });
        data.on('end', () => {
          res.end(chunks)
        });
    }).on('error',function(err){
        console.log(err)
    }).end();
})
//donations
s.getRequest = function(url,callback){
    return http.get(url, function(res){
        var body = '';
        res.on('data', function(chunk){
            body += chunk;
        });
        res.on('end',function(){
            try{body = JSON.parse(body)}catch(err){}
            callback(null,body)
        });
    }).on('error', function(e){
      callback(e)
//                              s.systemLog("Get Snapshot Error", e);
    });
}

app.get('/data/cameras/:file', function(req, res) {
    res.redirect('https://hub.shinobi.video/explore')
});
app.get('/data/:file', function(req, res) {
    req.file='data/'+req.params.file;
    fs.exists(req.file,function(exists){
        if(exists){
            fs.createReadStream(req.file).pipe(res).end()
        }else{
            res.send(JSON.stringify({ok:false,msg:'no file found'}));
            res.end();
        }
    })

});
app.get(['/docs/cameras','/docs/cameras/:file'], function(req, res) {
    res.redirect('https://hub.shinobi.video/explore')
})
app.get(['/docs','/docs/:file'], function(req, res) {
    req.pageDataFile='web/data/'+req.params.file+'.json';
    if(req.params.file&&fs.existsSync(req.pageDataFile)){
       try{req.pageData=JSON.parse(fs.readFileSync(req.pageDataFile,'utf8'));}catch(err){console.log(err)}
    }
    if(req.params.file){
        req.file=req.params.file
    }else{
        req.file='index';
    }
    res.render('docs/'+req.file,{config:config,pageData:req.pageData});
});
app.get(['/','/:file','/:file/:option'], function(req, res) {
    try{
        req.pageDataFile='web/data/'+req.params.file+'.json';
        if(req.params.file&&fs.existsSync(req.pageDataFile)){
           try{req.pageData=JSON.parse(fs.readFileSync(req.pageDataFile,'utf8'));}catch(err){console.log(err)}
        }
        switch(req.params.file){
            case'articles':
                req.file = 'articles'
                if(req.params.option && req.pageData[req.params.option]){
                    res.redirect('https://hub.shinobi.video/articles/view/' + req.pageData[req.params.option].id);
                }else{
                    res.redirect('https://hub.shinobi.video/articles')
                }
                return
            break;
            case'suggestions':
                req.file = 'suggestions'
                // if(req.params.option && req.pageData[req.params.option]){
                //     res.redirect('https://hub.shinobi.video/suggestions/view/' + req.pageData[req.params.option].id);
                // }else{
                    res.redirect('https://hub.shinobi.video/suggestions')
                // }
                return
            break;
            case'explore':
                req.file = 'explore'
                // if(req.params.option && req.pageData[req.params.option]){
                //     res.redirect('https://hub.shinobi.video/suggestions/view/' + req.pageData[req.params.option].id);
                // }else{
                    res.redirect('https://hub.shinobi.video/explore')
                // }
                return
            break;
            default:
                if(req.params.file&&req.params.file!=='cameras'){
                    req.file=req.params.file
                }else{
                    req.file='index';
                }
            break;
        }
        req.data={config:config,pageData:req.pageData,file_get_contents:s.file_get_contents,__dirname:__dirname,option:req.params.option}
        res.render('pages/'+req.file,req.data)
    }catch(err){
        res.render('pages/404',req.data)
    }
});
// var getUpvotes = function(markdownBody){
//     var regex = /\>\*\*Upvotes : (.*?)\*\*/g;
//     var matched = regex.exec(markdownBody);
//     if(!matched)return null;
//     return parseInt(matched[1])
// }
// var getDownvotes = function(markdownBody){
//     var regex = /\>\*\*Downvotes : (.*?)\*\*/g;
//     var matched = regex.exec(markdownBody);
//     if(!matched)return null;
//     return parseInt(matched[1])
// }
// var setVotes = function(markdownBody,value,isDownvote){
//     var current
//     if(isDownvote){
//         current = getDownvotes(markdownBody)
//     }else{
//         current = getUpvotes(markdownBody)
//     }
//     console.log(current)
//     if(!current){
//         var values = {
//             Upvotes : 0,
//             Downvotes : 0,
//         }
//         var label = 'Upvotes'
//         if(isDownvote){
//             label = 'Downvotes'
//         }
//         if(value === -1){
//             values[label] = 0
//         }else{
//             values[label] = 1
//         }
//         markdownBody += '\n'
//         markdownBody += '>**Upvotes : '+values.Upvotes+'**'
//         markdownBody += '>**Downvotes : '+values.Downvotes+'**'
//     }else{
//         var newValue = current + value;
//         if(newValue < 0)newValue = 0;
//         markdownBody =  markdownBody.replace('>**'+label+' : '+current+'**','>**'+label+' : '+newValue+'**')
//     }
//     return markdownBody
// }
// app.get('/pendingtasks/:taskId/:action', function(req, res) {
//     var taskId = req.params.taskId
//     request.get('https://api.trello.com/1/cards/'+taskId+'?key='+config.trelloKey+'&token='+config.trelloToken+'&fields=desc', function(err,resp,data) {
//         var json = JSON.parse(data)
//         switch(req.params.action){
//             case'upvote':
//                 var newDesc = setVotes(json.desc,1)
//                 request.put('https://api.trello.com/1/cards/'+taskId+'?key='+config.trelloKey+'&token='+config.trelloToken+'&desc='+newDesc, function(err,resp,data) {
//                     console.log('edit',data)
//                 })
//                 s.closeJsonResponse(res,data)
//             break;
//             case'downvote':
//                 var newDesc = setVotes(json.desc,1,true)
//                 request.put('https://api.trello.com/1/cards/'+taskId+'?key='+config.trelloKey+'&token='+config.trelloToken+'&desc='+newDesc, function(err,resp,data) {
//                     console.log('edit',data)
//                 })
//                 s.closeJsonResponse(res,data)
//             break;
//         }
//     });
// });
app.post(['/:form','/shop/:form'],function(req,res){
    res.setHeader('Content-Type','application/json');
    req.reply=function(reply){
        res.end(s.s(reply, null, 3));
    }
    if(!s.database[req.params.form]){
        req.reply({ok:false,msg:'Not a valid form type'})
        return
    }
    Object.keys(req.body).forEach(function(v,n){
        if(!req.body[v]){return}
        req.body[v]=req.body[v].trim()
    })
    switch(req.params.form){
        case'contact':
            req.form={
                name:req.body.name,
                mail:req.body.mail,
                city:req.body.city,
                province:req.body.province,
                country:req.body.country,
                note:req.body.note
            }
        break;
        case'feature-request':
            req.form={
                name:req.body.name,
                mail:req.body.mail,
                country:req.body.country,
                title:req.body.title,
                note:req.body.note
            }
        break;
        default:
            req.form={
                orderID:req.body.orderID,
                mail:req.body.mail,
                name:req.body.name,
                address:req.body.address,
                city:req.body.city,
                province:req.body.province,
                country:req.body.country,
                phone:req.body.phone,
                note:req.body.note
            }
            if(req.form.orderID)req.form.orderID=req.body.orderID.replace(/ /g,'');
            req.checkValue=req.body.orderID;
        break;
    }
    if(req.body.orderID===''){
        req.reply({ok:false,msg:'Order ID cannot be empty'})
        return
    }
    if(req.body.name===''){
        req.reply({ok:false,msg:'Name cannot be empty'})
        return
    }
    if(req.body.mail===''){
        req.reply({ok:false,msg:'Email cannot be empty'})
        return
    }
    if(req.body.address&&req.body.address===''){
        req.reply({ok:false,msg:'Address cannot be empty'})
        return
    }
    if(req.body.city&&req.body.city===''){
        req.reply({ok:false,msg:'City cannot be empty'})
        return
    }
    if(req.body.province&&req.body.province===''){
        req.reply({ok:false,msg:'Province cannot be empty'})
        return
    }
    if(req.body.country&&req.body.country===''){
        req.reply({ok:false,msg:'Country cannot be empty'})
        return
    }
    if(req.body.phone&&req.body.phone!==''){
        req.reply({ok:true,msg:'Sent'})
        return
    }
    req.next=function(){
        var html = ''
        var discordMail = ''
        Object.keys(req.form).forEach(function(v,n){
            discordMail += '*' + v + '* : ' + req.form[v] + '\n'
            html += '<div><b>'+v+' :</b> '+req.form[v]+'</div>'
        })
        s.sendDiscordMail({
            author: {
              name: req.form.name
            },
            description: discordMail
        })
        if(nodemailer){
            req.mailOptions = {
                from: '"ShinobiCCTV" <no-reply@shinobi.video>',
                to: config.mail.auth.user,
                subject: '"'+req.params.form+'" Request from '+req.form.name,
                html: html,
            };
            nodemailer.sendMail(req.mailOptions, (error, info) => {
                if (error) {
                    console.log(req.form)
                    console.log(req.mailOptions.html)
                    console.log(error)
                }
            });
        }
        req.reply({ok:true})
    }
    if(req.checkValue){
        s.database[req.params.form].find({orderID:new RegExp(req.checkValue, "g")}, function (err, docs) {
            if(docs.length===0){
                s.database[req.params.form].insert(req.form, function (err, newDoc) {
                    if(err)console.log(err);
                    req.next()
                });
            }else{
                req.reply({ok:false,msg:'Order ID Exists'})
            }
        });
    }else{
        req.next()
    }
})
//start server
app.listen(config.port,config.ip,function () {
  console.log('Website Loaded on port '+config.port)
});
exec('pm2 flush',{detached:true})
setTimeout(function(){
    exec('pm2 flush',{detached:true})
},60000*60*2)
